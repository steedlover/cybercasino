import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ContentLayoutComponent } from "./layout/content-layout/content-layout.component";

const routes: Routes = [
  // COMMENT
  // We could have here different layout such as Auth, Error etc.
  {
    path: "",
    component: ContentLayoutComponent,
    children: [
      {
        path: "",
        loadChildren: () =>
          import("./modules/home/home.module").then((m) => m.HomeModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
