import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CategoryComponent } from "./page/category/category.component";
import { GameComponent } from "./page/game/game.component";

export const routes: Routes = [
  {
    path: "",
    redirectTo: "category",
    pathMatch: "full",
  },
  {
    path: "category",
    component: CategoryComponent,
  },
  {
    path: "category/:slug",
    component: CategoryComponent,
  },
  {
    path: "category/:slug/:id",
    component: GameComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
