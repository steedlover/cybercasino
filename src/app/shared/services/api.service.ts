import { ApiData } from "../static.data";

export class ApiService {
  private buildUrl(...args): string {
    let result = ApiData.baseUrl,
      extra = [];
    args.forEach((e) => (result += "/" + e));
    Object.keys(ApiData.extraParams).forEach((key) =>
      extra.push(key + "=" + ApiData.extraParams[key])
    );
    return result + (extra ? "?" + extra.join("&") : "");
  }

  public getCategoriesUrl(): string {
    return this.buildUrl(ApiData.categoriesParam);
  }

  public getCategoryBySlugUrl(slug: string): string {
    return this.buildUrl(ApiData.categoriesParam, slug);
  }

  public getGameByIDUrl(id: string): string {
    return this.buildUrl(ApiData.gamesParam, id);
  }
}
