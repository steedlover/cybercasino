import { Component, Input, Output, OnInit, EventEmitter } from "@angular/core";

@Component({
  selector: "app-burger-button",
  templateUrl: "./burger-button.component.html",
  styleUrls: ["./burger-button.component.scss"],
})
export class BurgerButtonComponent implements OnInit {
  @Input() active: boolean;
  @Output() clicked = new EventEmitter<boolean>();

  constructor() {}

  onClick() {
    this.clicked.emit(!this.active);
  }

  ngOnInit() {}
}
