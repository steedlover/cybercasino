import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Game } from "../../../../data/interfaces/game";
import { GameCategory } from "../../../../data/interfaces/game-category";
import { GameCategoryService } from "../../../../data/services/game-category.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-category",
  templateUrl: "./category.component.html",
  styleUrls: ["./category.component.scss"],
})
export class CategoryComponent implements OnInit {
  private alias: string;
  private category: GameCategory;
  private allGames: Game[];
  private games: Game[];
  private sub: Subscription;

  constructor(
    private catService: GameCategoryService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.sub = catService.loadingChanged$.subscribe((response) => {
      if (response === false) {
        this.setRouteListener();
        this.getDefaultData();
      }
    });
  }

  checkGames(): boolean {
    return this.games && this.games.length > 0 ? true : false;
  }

  private setRouteListener() {
    // COMMENT
    // Unsubscribing from the very first observing
    // otherwise all the time the subscription
    // will be triggering on the loading state changing
    // and fetch the static object
    this.sub.unsubscribe();

    this.route.paramMap.subscribe((paramMap) => {
      if (paramMap.get("slug") !== this.alias) {
        this.alias = paramMap.get("slug");
        console.log("serv on subscribe", this.alias);
        this.catService.setActiveCategory(this.alias);
        this.getData();
      }
    });
  }

  private setGames(arr: Game[]) {
    this.allGames = arr;
    this.games = this.allGames.filter((el, i) => i < 20);
  }

  private getDefaultData() {
    const serverData = this.catService.getStaticData();
    this.category = this.alias
      ? serverData.filter((e) => this.alias === e.slug)[0]
      : serverData[0];
    this.setGames(this.category._embedded.games);
  }

  private getData() {
    this.catService.getCategoryBySlug(this.alias).then((response) => {
      this.category = response;
      this.setGames(this.category._embedded.games);
    });
  }

  getBGImage(game: Game): string {
    return 'url("' + game.thumbnail + '")';
  }

  showMore() {
    alert(":) Later");
  }

  ngOnInit() {
    this.alias = this.route.snapshot.paramMap.get("slug");
    this.catService.setActiveCategory(this.alias);
    // COMMENT
    // if service loading false, the back button was pushed
    // I'm requesting the cache data
    if (this.catService.loading === false) {
      this.getData();
    }
  }
}
