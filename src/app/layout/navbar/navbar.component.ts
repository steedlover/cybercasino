import { Component, OnInit } from "@angular/core";
import { GameCategoryService } from "../../data/services/game-category.service";
import { GameCategory } from "../../data/interfaces/game-category";
import { CategoryIcons } from "../../shared/static.data";
import { Router } from "@angular/router";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
  private mobileMenuActive: boolean = false;
  private categories: GameCategory[] = [];
  private activeCategory: string;

  constructor(
    private catService: GameCategoryService,
    private router: Router
  ) {}

  ngOnInit() {
    this.catService.getAllCategories().then((arr: GameCategory[]) => {
      this.categories = arr;
      console.log(this.categories, "cats in component");
    });
    this.catService.activeCategoryChanged$.subscribe(
      (name) => (this.activeCategory = name)
    );
  }

  burgerClicked(s: boolean) {
    if (s !== this.mobileMenuActive) {
      this.mobileMenuActive = s;
    }
  }

  navigate(...args) {
    this.mobileMenuActive = false;
    this.router.navigate(args);
  }

  getCategoryIcon(slug: string): string {
    return CategoryIcons[slug];
  }
}
