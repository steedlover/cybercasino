import {
  GameCategory,
  GameCategoryResponse,
} from "../interfaces/game-category";
import { Injectable } from "@angular/core";
import { Observable, throwError, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { ApiService } from "../../shared/services/api.service";

@Injectable()
export class GameCategoryService {
  //private categoriesList: GameCategory[] = [
  //{ name: "Game", slug: "game", order: 1, _embedded: { games: [] } },
  //{ name: "Soccer", slug: "soccer", order: 2, _embedded: { games: [] } },
  //{ name: "Chess", slug: "chess", order: 3, _embedded: { games: [] } },
  //];
  private categoriesList: GameCategory[];
  private _loading: boolean = false;
  private loadingChangedSource = new Subject<boolean>();
  public loadingChanged$ = this.loadingChangedSource.asObservable();
  private activeCategoryChangedSource = new Subject<string>();
  public activeCategoryChanged$ = this.activeCategoryChangedSource.asObservable();

  constructor(private http: HttpClient, private api: ApiService) {}

  set loading(newVal: boolean) {
    if (newVal !== this._loading) {
      this._loading = newVal;
      this.loadingChangedSource.next(this._loading);
    }
  }

  get loading(): boolean {
    return this._loading;
  }

  private fetchCategories(): Observable<GameCategory[]> {
    console.log("service fetch categories");
    return this.http.get<GameCategory[]>(this.api.getCategoriesUrl());
  }

  private fetchCategoryBySlug(slug: string): Observable<GameCategory[]> {
    return this.http.get<GameCategory[]>(this.api.getCategoryBySlugUrl(slug));
  }

  public getStaticData(): GameCategory[] {
    return this.categoriesList;
  }

  public setActiveCategory(name?: string) {
    if (name) {
      this.activeCategoryChangedSource.next(name);
    }
  }

  public getCategoryBySlug(slug: string): Promise<GameCategory> {
    return new Promise((resolve, reject) => {
      setTimeout(() => (this.loading = true), 0);
      this.fetchCategoryBySlug(slug).subscribe((data: any) => {
        this.loading = false;
        resolve(data);
      });
    });
  }

  public getAllCategories(): Promise<GameCategory[]> {
    return new Promise((resolve, reject) => {
      if (!this.loading) {
        this.loading = true;
        if (this.categoriesList) {
          this.loading = false;
          resolve(this.categoriesList);
        } else {
          this.fetchCategories().subscribe(
            (data: any) => {
              if (data._embedded && data._embedded.game_categories.length > 0) {
                this.categoriesList = data._embedded.game_categories;
                this.loading = false;
                resolve(this.categoriesList);
              }
            },
            (err) => {
              console.warn("Something went wrong on fetching the data", err);
              this.loading = false;
              reject(err);
            }
          );
        }
      } else {
        this.loadingChangedSource.subscribe((e) => {
          if (e === false) {
            resolve(this.categoriesList);
          }
        });
      }
    });
  }
}
