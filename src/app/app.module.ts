import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { GameCategoryService } from "./data/services/game-category.service";
import { GameService } from "./data/services/game.service";
import { ApiService } from "./shared/services/api.service";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { SharedModule } from "./shared/shared.module";

import { ContentLayoutComponent } from "./layout/content-layout/content-layout.component";
import { NavbarComponent } from "./layout/navbar/navbar.component";
import { BurgerButtonComponent } from "./layout/navbar/burger-button/burger-button.component";
import { SideBarComponent } from "./layout/side-bar/side-bar.component";

@NgModule({
  declarations: [
    AppComponent,
    ContentLayoutComponent,
    NavbarComponent,
    BurgerButtonComponent,
    SideBarComponent,
  ],
  imports: [BrowserModule, HttpClientModule, SharedModule, AppRoutingModule],
  providers: [GameService, GameCategoryService, ApiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
