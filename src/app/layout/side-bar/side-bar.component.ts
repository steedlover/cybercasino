import { Component, OnInit } from "@angular/core";
import { GameCategoryService } from "../../data/services/game-category.service";
import { GameCategory } from "../../data/interfaces/game-category";
import { CategoryIcons } from "../../shared/static.data";
import { Router } from "@angular/router";

@Component({
  selector: "app-side-bar",
  templateUrl: "./side-bar.component.html",
  styleUrls: ["./side-bar.component.scss"],
})
export class SideBarComponent implements OnInit {
  private categories: GameCategory[] = [];
  private activeCategory: string;

  constructor(
    private catService: GameCategoryService,
    private router: Router
  ) {}

  ngOnInit() {
    this.catService.getAllCategories().then((arr: GameCategory[]) => {
      this.categories = arr;
      console.log(this.categories, "cats in component");
    });
    this.catService.activeCategoryChanged$.subscribe(
      (name) => (this.activeCategory = name)
    );
  }

  navigate(...args) {
    this.router.navigate(args);
  }

  getCategoryIcon(slug: string): string {
    return CategoryIcons[slug];
  }
}
