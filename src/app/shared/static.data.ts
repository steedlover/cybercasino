export const ApiData = {
  //baseUrl: "https://staging-frontapi.cherrytech.com",
  baseUrl: "https://cherrycasino-frontapi.cherrytech.com",
  categoriesParam: "game-categories",
  gamesParam: "games",
  extraParams: {
    brand: "cherrycasino.desktop",
    locale: "en",
  },
};

export const CategoryIcons = {
  "popular-games": "exclamation-triangle",
  "video-slots": "check",
  "table-games": "square",
  "video-poker": "paint-brush",
  "classic-slots": "book",
  "jackpot-games": "language",
  "other-games": "tasks",
  "all-games": "user-circle",
};
