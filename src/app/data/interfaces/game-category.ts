import { Game } from "./game";

export interface GameCategory {
  name: string;
  slug: string;
  order: number;
  _embedded: {
    games: Game[];
  };
}

export interface GameCategoryResponse {
  total_items: number;
  _embedded: {
    game_categories: GameCategory[];
  };
}
