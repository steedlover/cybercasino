import { NgModule } from "@angular/core";

import { CategoryComponent } from "./page/category/category.component";
import { GameComponent } from "./page/game/game.component";

import { HomeRoutingModule } from "./home.routing";

import { SharedModule } from "../../shared/shared.module";

@NgModule({
  declarations: [CategoryComponent, GameComponent],
  imports: [SharedModule, HomeRoutingModule],
})
export class HomeModule {}
