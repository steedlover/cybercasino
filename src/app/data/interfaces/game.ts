export interface Game {
  id: string;
  name: string;
  slug: string;
  thumbnail?: string;
  background?: string;
  description?: string;
  width: string;
  height: string;
}
