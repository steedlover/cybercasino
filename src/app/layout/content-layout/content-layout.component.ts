import { Component } from "@angular/core";
import { GameCategoryService } from "../../data/services/game-category.service";

@Component({
  selector: "app-content-layout",
  templateUrl: "./content-layout.component.html",
  styleUrls: ["./content-layout.component.scss"],
})
export class ContentLayoutComponent {
  private loading: boolean;

  constructor(private catService: GameCategoryService) {
    // COMMENT
    // Here I spent 3 hours when I got the error
    // "Expression has changed after it was checked":
    // and old tricks still work :)
    setTimeout(() => {
      this.loading = catService.loading;
      catService.loadingChanged$.subscribe((response) => {
        //console.log("layout loading changed", response);
        this.loading = response;
      });
    }, 0);
  }
}
