import { Game } from "../interfaces/game";
import { Injectable } from "@angular/core";
import { Observable, throwError, Subject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { GameCategoryService } from "./game-category.service";
import { ApiService } from "../../shared/services/api.service";

@Injectable()
export class GameService {
  private game: Game;
  constructor(private http: HttpClient, private api: ApiService) {}

  private fetchGameByID(id: string): Observable<Game> {
    console.log("service fetch game");
    return this.http.get<Game>(this.api.getGameByIDUrl(id));
  }

  public getGameByID(id: string): Promise<Game> {
    return new Promise((resolve, reject) => {
      this.fetchGameByID(id).subscribe(
        (data: any) => {
          console.log(data, "game response");
          resolve();
        },
        (err) => reject(err)
      );
    });
  }
}
