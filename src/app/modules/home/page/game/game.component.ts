import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { Game } from "../../../../data/interfaces/game";
import { GameCategoryService } from "../../../../data/services/game-category.service";
import { GameService } from "../../../../data/services/game.service";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"],
})
export class GameComponent implements OnInit {
  private categoryAlias: string;
  private gameID: string;
  private error: string;
  private game: Game;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private gameService: GameService,
    private catService: GameCategoryService
  ) {}

  goBack() {
    this.router.navigate(["category", this.categoryAlias]);
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      if (paramMap.get("id") !== this.gameID) {
        this.gameID = paramMap.get("id");
        this.categoryAlias = paramMap.get("slug");
        this.catService.setActiveCategory(this.categoryAlias);
        setTimeout(() => {
          this.catService.loading = true;
        }, 0);
        this.gameService
          .getGameByID(this.gameID)
          .then((r) => {
            this.error = "";
            this.catService.loading = false;
            console.log(r, "response");
          })
          .catch((data) => {
            this.catService.loading = false;
            if (data.status === 404) {
              this.error = "The slot was not found";
            }
          });
      }
    });
  }
}
